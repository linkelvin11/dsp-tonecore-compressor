# README #

This repository contains code for a compressor effect in the DSP Tonecore effects pedal

### What is this repository for? ###

v0.1
Basic compressor functionality is present. The compressor has 3 knob adjustments: level, threshold, attack, and ratio. Using these three knobs the behaviour and amount of compression can be adjusted

### How do I get set up? ###

Assemble the source through cmd using the following commands:
ASM56300.EXE -l -bcompressor.cln compressor.asm
DSPLINK.EXE -bcompressor.cld compressor.cln

The resulting compressor.cld binary can be loaded into the pedal through the Tonecore design software.

### Who do I talk to? ###

admin: linkelvin11